# Kyoiku-Kanji Study App (Web Scraping + Google Sheets + Vue + Axios)

Content/Inspiration/Tools:

https://en.wikipedia.org/wiki/Ky%C5%8Diku_kanji

https://source.unsplash.com/

https://vuejs.org/

https://support.google.com/docs/answer/3093339?hl=en

http://gsx2json.com/

https://medium.com/techtrument/handling-ajax-request-in-vue-applications-using-axios-1d26c47fab0

https://codepen.io/RuudBurger/pen/bwjry?editors=1100


> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```